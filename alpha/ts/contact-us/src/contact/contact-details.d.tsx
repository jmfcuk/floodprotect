export interface ContactDetails {

      salutation: string;
      fname: string;
      lname: string;
      organization: string;
      email: string;
      phone: string;
      bestTime: string;
      street: string;
      city: string;
      subscribed: boolean;
      postcode: string;
      message: string;
}