import * as React from 'react';
import { Row, Col, Modal, Button } from 'react-bootstrap';
import TextareaAutosize from 'react-autosize-textarea';
import { ContactDetails } from './contact-details.d';
import { baseApiUrl } from '../global';
import PrivacyNotice from './privacy-notice';
var Reaptcha = require('reaptcha');

interface ContactFormProps {
    onSend: (data: ContactDetails) => void;
}

interface ContactFormState {
    contactDetails: ContactDetails;
    captchaVerified: boolean;
    showPrivacy: boolean;
}

class ContactForm extends React.Component<ContactFormProps, ContactFormState> {

    state: ContactFormState;

    constructor(props: ContactFormProps) {
        super(props);

        this.state = {
            contactDetails: {
                salutation: '',
                fname: '',
                lname: '',
                organization: '',
                email: '',
                phone: '',
                bestTime: 'never',
                street: '',
                city: '',
                postcode: '',
                subscribed: false,
                message: ''
            },
            captchaVerified: true,
            showPrivacy: false
        };
    }

    salutationChange = (e: any) => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.salutation = e.target.value;

        this.setState({ contactDetails: cd });
    }

    fnameChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.fname = e.target.value;

        this.setState({ contactDetails: cd });
    }

    lnameChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.lname = e.target.value;

        this.setState({ contactDetails: cd });
    }

    organizationChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.organization = e.target.value;

        this.setState({ contactDetails: cd });
    }

    emailChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.email = e.target.value;

        this.setState({ contactDetails: cd });
    }

    phoneChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.phone = e.target.value;

        this.setState({ contactDetails: cd });
    }

    bestTimeChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.bestTime = e.target.value;

        this.setState({ contactDetails: cd });
    }

    streetChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.street = e.target.value;

        this.setState({ contactDetails: cd });
    }

    cityChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.city = e.target.value;

        this.setState({ contactDetails: cd });
    }

    postcodeChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.postcode = e.target.value;

        this.setState({ contactDetails: cd });
    }

    messageChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.message = e.target.value;

        this.setState({ contactDetails: cd });
    }

    subscribedChange = (e: any): void => {

        let cd: ContactDetails = this.state.contactDetails;
        cd.subscribed = e.target.checked;

        this.setState({ contactDetails: cd });
    }

    isNullOrWhitespace = (str: string) => {

        return !str || !str.trim();
    }

    onRcVerify = (value: string) => {

        const url = baseApiUrl + '/rc-validate.php';

        const data = {
            response: value
        };

        const body = JSON.stringify(data);

        fetch(url, {
            method: 'POST',
            mode: 'cors',
            body: body
        })
            .then(response => {
                return response.json();
            })
            .then(json => {
                if (json.success) {
                    this.setState({ captchaVerified: true });
                }
            });
    }

    onSend = () => {

        const data: ContactDetails = this.state.contactDetails;

        this.props.onSend(data);
    }

    togglePrivacy = () => {

        const { showPrivacy } = this.state;

        this.setState({ showPrivacy: !showPrivacy });
    }

    render(): JSX.Element {

        return (
            <React.Fragment>
                <Row>
                    <div className="form-group">
                        <Row>
                            <div className="text-center">
                                <p><i>Fill out your details below and we will get back to you.
                                Only a first name and email are required.</i></p>
                            </div>
                        </Row>
                        <Row>
                            <Col xs={2}>
                                <label htmlFor="salutation">Title</label>
                                <select
                                    id="salutation"
                                    className="form-control"
                                    onChange={this.salutationChange}
                                >
                                    <option value="">Title</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                    <option value="Sir">Sir</option>
                                    <option value="Rev">Rev</option>
                                </select>
                            </Col>
                            <Col xs={5}>
                                <label htmlFor="fname">First Name</label>
                                <input
                                    id="fname"
                                    className="form-control"
                                    placeholder="First Name"
                                    onChange={this.fnameChange}
                                />
                            </Col>
                            <Col xs={5}>
                                <label htmlFor="lname">Last Name</label>
                                <input
                                    id="lname"
                                    className="form-control"
                                    placeholder="Last Name"
                                    onChange={this.lnameChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <label htmlFor="organization">Organisation</label>
                                <input
                                    id="organization"
                                    className="form-control"
                                    placeholder="Organization"
                                    onChange={this.organizationChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4}>
                                <label htmlFor="email">Email</label>
                                <input
                                    id="email"
                                    type="email"
                                    className="form-control"
                                    placeholder="Email"
                                    onChange={this.emailChange}
                                />
                            </Col>
                            <Col xs={4}>
                                <label htmlFor="phone">Phone</label>
                                <input
                                    id="phone"
                                    type="tel"
                                    className="form-control"
                                    placeholder="Phone"
                                    onChange={this.phoneChange}
                                />
                            </Col>
                            <Col xs={4}>
                                <div className="pull-right">
                                    <label htmlFor="bestTime">Best Time to call</label>
                                    <select className="form-control" onChange={this.bestTimeChange}>
                                        <option value="never">Never</option>
                                        <option value="any (09:00 - 20:00)">Any Time (09:00 - 20:00)</option>
                                        <option value="am (09:00 - 12:00)">Morning (09:00 - 12:00)</option>
                                        <option value="pm (12:00 - 17:00">Afternoon (12:00 - 17:00)</option>
                                        <option value="eve (17:00 - 20:00)">Evening (17:00 - 20:00)</option>
                                    </select>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <label htmlFor="street">Street</label>
                                <input
                                    id="street"
                                    className="form-control"
                                    placeholder="Street"
                                    onChange={this.streetChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={6}>
                                <label htmlFor="city">City</label>
                                <input
                                    id="city"
                                    className="form-control"
                                    placeholder="City"
                                    onChange={this.cityChange}
                                />
                            </Col>
                            <Col xs={6}>
                                <label htmlFor="postcode">Postcode</label>
                                <input
                                    id="postcode"
                                    className="form-control"
                                    placeholder="Postcode"
                                    onChange={this.postcodeChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <label htmlFor="message">Message</label>
                                <TextareaAutosize
                                    id="message"
                                    className="form-control"
                                    placeholder="Anything you want to tell or ask us?"
                                    maxLength={1024}
                                    onChange={this.messageChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={6}>
                                <div>
                                    By providing your contact information, you are confirming you are an adult 18 years or
                                    older and you authorize Floodprotect to contact you by email or telephone with information
                                    about Floodprotect products, events, and updates.
                                    You may unsubscribe at any time by clicking the link provided in our communications.
                  Please review our <a style={{ cursor: 'pointer' }} onClick={this.togglePrivacy}>Privacy Notice</a> for more information.
              </div>
                            </Col>
                            <Col xs={6}>
                                <Row>
                                    <Col xs={6}>
                                        <label htmlFor="chkSubscribe" className="form-check-label">Subscribe for updates</label>
                                    </Col>
                                    <Col xs={6} className="pull-left">
                                        <input
                                            id="chkSubscribe"
                                            type="checkbox"
                                            checked={this.state.contactDetails.subscribed}
                                            onChange={this.subscribedChange}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Reaptcha sitekey="6LdTc4UUAAAAAAvEvwvT0OuSMX2laK3sK4Dr9FgI" onVerify={this.onRcVerify} />
                                </Row>
                                <Row>
                                    <button
                                        type="button"
                                        className="btn btn-primary btn-block"
                                        onClick={this.onSend}
                                        disabled={this.isNullOrWhitespace(this.state.contactDetails.fname) ||
                                            this.isNullOrWhitespace(this.state.contactDetails.email) ||
                                            !this.state.captchaVerified}
                                    >
                                        Send
                                    </button>
                                </Row>
                            </Col>
                        </Row>
                    </div>
                </Row>

                <Modal show={this.state.showPrivacy} onHide={this.togglePrivacy}>
                    <Modal.Header closeButton={true}>
                        <Modal.Title>Privacy Notice</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <PrivacyNotice />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.togglePrivacy}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        );
    }
}

export default ContactForm;
