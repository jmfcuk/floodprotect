import React, { Component } from 'react';
import FMap from './fmap';

export default class FMapContainer extends Component {

    state = {
        markerCount: 0,
        overlayCount: 0,
        deleteMode: false,
        processMarker: false,
        processOverlay: false
    }

    markerCountChanged = (markerCount) => {
        this.setState({ markerCount: markerCount });
    }

    overlayCountChanged = (overlayCount) => {
        this.setState({ overlayCount: overlayCount });
    }

    toggleProcessMarker = () => {

        const { processMarker } = this.state;

        this.setState({ processMarker: !processMarker });
    }

    toggleProcessOverlay = () => {

        const { processOverlay } = this.state;

        this.setState({ processOverlay: !processOverlay });
    }

    toggleDeleteMode = () => {

        const { deleteMode } = this.state;

        this.setState({ deleteMode: !deleteMode });
    }

    render() {

        return (
            <div>
                <div>
                    <div>
                        Markers:&nbsp;{this.state.markerCount}
                        <input type="checkbox" onClick={e => this.toggleProcessMarker()} />
                    </div>
                    <div>
                        Overlays:&nbsp;{this.state.overlayCount}
                        <input type="checkbox" onClick={e => this.toggleProcessOverlay()} />
                    </div>

                    <div>Delete:&nbsp;<input type="checkbox"
                        onClick={e => this.toggleDeleteMode()} />
                    </div>
                </div>
                <div>
                    <FMap
                        center={this.props.center}
                        markerCountChanged={this.markerCountChanged}
                        overlayCountChanged={this.overlayCountChanged}
                        processMarker={this.state.processMarker}
                        processOverlay={this.state.processOverlay}
                        deleteMode={this.state.deleteMode}
                    />
                </div>
            </div>
        );
    }
}
