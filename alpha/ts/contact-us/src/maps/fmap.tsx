import * as React from 'react';
const Map = require('pigeon-maps');
// const Overlay = require('pigeon-overlay');
// const Marker = require('pigeon-marker');
// const uniqueid = require('uniqid');

interface FMapProps {
    center: Array<number>;
    processMarker: boolean;
    processOverlay: boolean;
    deleteMode: boolean;
}

interface FMapState {
    markers: Array<any>;
    overlays: Array<any>;
}

export default class FMap extends React.Component<FMapProps, FMapState> {

    state = {
        markers: [],
        overlays: [],
    }

    onMapClick = (event: any) => {

        // const { processMarker, processOverlay, deleteMode } = this.props;

        const { deleteMode } = this.props;

        if (!deleteMode) {

            // const ll = event.latLng;

            // if (processMarker) {
            //     this.createMarker(ll);
            // }

            // if (processOverlay) {
            //     this.createOverlay(ll);
            // }
        }
    }

    // createMarker = (ll) => {

    //     const { markerCountChanged, processMarker } = this.props;

    //     if (processMarker) {

    //         const uid = uniqueid();

    //         const m = (<Marker
    //             key={uid}
    //             anchor={ll}
    //             payload={1}
    //             onClick={(e: any) => this.onMarkerClick(uid)}
    //         />);

    //         let ms = [...this.state.markers];

    //         ms.push({ id: uid, latLng: ll, m: m });

    //         this.setState({ markers: ms }, () => {
    //             markerCountChanged(ms.length);
    //         });
    //     }
    // }

    // onMarkerClick = (uid) => {

    //     const { markerCountChanged, processMarker, deleteMode } = this.props;

    //     if (processMarker && deleteMode) {

    //         const ms = [...this.state.markers];

    //         let newMs = ms.filter((m) => {
    //             if (m.id === uid) {
    //                 return false;
    //             }
    //             return true;
    //         });

    //         this.setState({ markers: newMs }, () => {
    //             markerCountChanged(newMs.length);
    //         });
    //     }
    // }


    // createOverlay = (ll) => {

    //     const { overlayCountChanged, processOverlay } = this.props;

    //     if (processOverlay) {

    //         const uid = uniqueid();

    //         const ol = (<Overlay
    //             key={uid}
    //             anchor={ll}
    //             offset={[0, 0]}
    //         >
    //             <div onClick={e => this.onOverlayClick(uid)}>
    //                 <img
    //                     src='img/unionjack-fg.jpg'
    //                     width={40}
    //                     height={20}
    //                     alt="overlay icon"
    //                 />
    //             </div>
    //         </Overlay>);

    //         let ols = [...this.state.overlays];

    //         ols.push({ id: uid, latLng: ll, ol: ol });

    //         this.setState({ overlays: ols }, () => {
    //             overlayCountChanged(ols.length);
    //         });
    //     }
    // }

    // onOverlayClick = (uid) => {

    //     const { overlayCountChanged, processOverlay, deleteMode } = this.props;

    //     if (processOverlay && deleteMode) {

    //         const ols = [...this.state.overlays];

    //         let newOls = ols.filter((ol) => {
    //             if (ol.id === uid) {
    //                 return false;
    //             }
    //             return true;
    //         });

    //         this.setState({ overlays: newOls }, () => {
    //             overlayCountChanged(newOls.length);
    //         });
    //     }
    // }

    render() {

        const { center } = this.props;
        // const ms = [...this.state.markers];
        // const ols = [...this.state.overlays];

        return (
            <div>
                <Map
                    center={center}
                    zoom={12}
                    width={600}
                    height={400}
                    onClick={(e: any) => this.onMapClick(e)}
                >
                    {/* {
                        ols && ols.map((ol) => {
                            return (ol.ol);
                        })
                    }
                    {
                        ms && ms.map((m) => {
                            return (m.m);
                        })
                    } */}
                </Map>
            </div>
        )
    }
}
