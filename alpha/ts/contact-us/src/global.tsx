export const baseApiUrl = 'http://localhost:9000';

export
    enum ViewType {
    overview, areasAndWarnings, contactUs
};

export const isNullOrWhitespace = (str: string) => {

    return !str || !str.trim();
}

