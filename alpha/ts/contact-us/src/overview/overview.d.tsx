
export interface OverviewHeaderDef {
  id: number;
  category: string;
  name: string;
  description: string;
  itemUri: string;
  imageUri: string;
}

export interface OverviewDef extends OverviewHeaderDef {
    data: object;
}

export default OverviewDef;
