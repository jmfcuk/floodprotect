import * as React from 'react';
import ItemDef from './overview.d';
import {
    Carousel,
    CarouselItem
} from 'react-bootstrap';

interface ItemsProps {
    items: Array<ItemDef>;
}

const OverviewSet = (props: ItemsProps) => {

    const { items } = props;

    return (
            <Carousel
                slide={true}
                controls={true}
                indicators={false}
                interval={0}
            >
                {items && items.map((item: ItemDef) => (
                    <CarouselItem key={item.id}>
                        <img className="img-responsive center-block" src={item.imageUri} />
                    </CarouselItem>
                ))}
            </Carousel>
    );
};

export default OverviewSet;
