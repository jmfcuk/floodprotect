import * as React from 'react';
import {
    Row
} from 'react-bootstrap';
import OverviewDef from './overview.d';
import OverviewSet from './overview-set';

interface OverviewProps {

}

interface OverviewState {
    items: Array<OverviewDef>;
}

export default class Overview extends React.Component<OverviewProps, OverviewState> {

    state: OverviewState;

    constructor(props: OverviewProps) {

        super(props);

        this.state = {
            items: [],
        };
    }

    componentDidMount() {
        this.getItems();
    }

    getItems = (reset: boolean = false) => {

        const url = 'data/items.json';

        fetch(url)
            .then(response => {
                return response.json();
            })
            .then(its => {
                this.setState({ items: its });
            });
    }

    render() {
        return (
            <Row className="text-center">
                <OverviewSet items={this.state.items} />
            </Row >
        );
    }
}


