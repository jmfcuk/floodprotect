import * as React from 'react';
import {
    Row,
    Col,
    ListGroup,
    ListGroupItem,
} from 'react-bootstrap';
import Filter from './filter';
import AreaDetail from './area-detail';
const uniqueid = require('uniqid');

interface FloodAreasProps {
    floodAreas: any;
}

interface FloodAreaState {
    counties: Array<string>;
    filteredCounties: Array<string>;
    selectedCounty: string;
    countyAreas: Array<any>;
    filteredCountyAreas: Array<any>;
    selectedCountyArea: any;
}

export default class FloodAreas extends React.Component<FloodAreasProps, FloodAreaState> {

    state: FloodAreaState = {
        counties: [],
        selectedCounty: '',
        filteredCounties: [],
        countyAreas: [],
        filteredCountyAreas: [],
        selectedCountyArea: null
    };

    constructor(props: FloodAreasProps) {
        super(props);

        fetch('data/uk-counties.json')
            .then(response => {
                return response.json();
            })
            .then(json => {

                const noSeperatorLines = json.filter((line: string) => {
                    return (line !== '--------');
                });

                this.setState({
                    counties: noSeperatorLines,
                    filteredCounties: noSeperatorLines
                });
            });
    }

    onCountyFilterChange = (e: any) => {

        const mask = e.target.value.toLowerCase();

        const { counties } = this.state;

        const fc = counties.filter((c) => {
            if (c.toLowerCase().includes(mask)) {
                return true;
            }
            return false;
        });

        this.setState({ filteredCounties: fc });
    }

    onSelectCounty = (c: string) => {

        let items = [];

        if (this.props.floodAreas) {

            items = this.props.floodAreas.items.filter((item: any) => {
                return item.county.toLowerCase().includes(c.toLowerCase());

            });
        }

        this.setState({
            selectedCounty: c,
            countyAreas: items,
            filteredCountyAreas: items,
            selectedCountyArea: null
        });
    }

    onCountyAreaFilterChange = (e: any) => {

        const mask = e.target.value.toLowerCase();

        const { countyAreas } = this.state;

        const fca = countyAreas.filter((ca) => {
            if (ca.description.toLowerCase().includes(mask)) {
                return true;
            }
            return false;
        });

        this.setState({ filteredCountyAreas: fca });
    }

    onSelectCountyArea = (ca: any) => {

        this.setState({ selectedCountyArea: ca });
    }

    render() {

        const { selectedCountyArea } = this.state;

        let content = null;

        if (selectedCountyArea) {
            content = (<AreaDetail item={selectedCountyArea} />);
        } else {
            content = (
                <Row>
                    <Col xs={12}>
                        <Filter
                            label={'Filter Areas (' + this.state.filteredCountyAreas.length + ')'}
                            onFilterChange={this.onCountyAreaFilterChange} />
                        <ListGroup>
                            {
                                this.state.filteredCountyAreas &&
                                this.state.filteredCountyAreas.map((ca: any) => {
                                    return (
                                        <ListGroupItem
                                            key={uniqueid()}
                                            onClick={e => { this.onSelectCountyArea(ca) }}>
                                            {ca.description}
                                        </ListGroupItem>
                                    )
                                })
                            }
                        </ListGroup>
                    </Col>
                </Row>
            )
        }

        return (
            <React.Fragment>
                <Row>
                    <Col xs={4}>
                        <Filter
                            label={'Filter Counties (' + this.state.filteredCounties.length + ')'}
                            onFilterChange={this.onCountyFilterChange} />
                        <ListGroup>
                            {
                                this.state.filteredCounties &&
                                this.state.filteredCounties.map((c: string) => {
                                    return (
                                        <ListGroupItem
                                            key={uniqueid()}
                                            onClick={e => this.onSelectCounty(c)}>
                                            {c}
                                        </ListGroupItem>
                                    )
                                })
                            }
                        </ListGroup>
                    </Col>
                    <Col xs={8}>
                        {content}
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}
