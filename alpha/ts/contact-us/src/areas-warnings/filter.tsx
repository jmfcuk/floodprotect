import * as React from 'react';
import { Row, Col } from 'react-bootstrap';

export const Filter = (props: any) => {

    return (
        <Row>
            <Col xs={12}>
                <label htmlFor="filterText">{props.label}</label>
                <input
                    id="filterText"
                    className="form-control"
                    placeholder="Containing..."
                    onChange={props.onFilterChange}
                />
            </Col>
        </Row>        
    );
}

export default Filter;
