import * as React from 'react';
import {
     Row,
     Carousel, CarouselItem
} from 'react-bootstrap';
import './ticker.css';
const uniqueid = require('uniqid');
const moment = require('moment');

interface FwProps {
    fw: any;
}

interface FwState {
    selected: any;
}

export default class FloodWarningsTicker extends React.Component<FwProps, FwState> {

    state: FwState;

    constructor(props: FwProps) {
        super(props);

        this.state = {
            selected: null
        };
    }

    itemSelect = (item: any) => {
        this.setState({ selected: item });
    }

    itemClose = () => {
        this.setState({ selected: null });
    }

    formatDateTime = (dt: any) => {

        let m = moment(dt);

        return m.format('dddd DD MMMM YYYY HH:mm:ss');
    }

    render() {

        const { fw } = this.props;
        //const { selected } = this.state;

        let content: JSX.Element;

        if (fw && fw.items) {
            if (fw.items.length > 0) {
                content =
                    (
                        <Carousel
                            slide={true}
                            controls={false}
                            indicators={false}
                            interval={2000}
                            pauseOnHover={false}
                        >
                            {
                                fw.items.map((item: any) => (
                                    <CarouselItem
                                        key={uniqueid()}
                                        className="list-group-horizontal scroll"
                                        style={{ cursor: 'pointer' }}
                                    >
                                        <span onClick={e => this.itemSelect(item)}>{item.description}</span>
                                    </CarouselItem>
                                ))}
                        </Carousel>
                    );
            } else {
                content = (<Row>There are no UK flood warnings right now</Row>);
            }
        } else {
            content = (<Row>No UK flood warning data is available right now</Row>);
        }

        return (
            <div>

                {content}

                {/* <Row>
                    <h4>UK Flood Warnings</h4>
                </Row>
                <Row>
                    <Col xs={2} />
                    <Col xs={8}>
                        {content}
                    </Col>
                    <Col xs={2} />
                </Row> */}

                {/* <Modal show={selected} onHide={this.itemClose}>
                    <Modal.Header closeButton={true}>
                        <Modal.Title>{selected && selected.description}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            {JSON.stringify(selected)}
                        </div>
                        <div>
                            <i>This uses Environment Agency flood and river level data from the real-time data API (Beta)</i>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.itemClose}>Close</Button>
                    </Modal.Footer>
                </Modal> */}

            </div>
        );
    }
}
