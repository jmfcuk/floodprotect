import * as React from 'react';
import { Row, Col } from 'react-bootstrap';
import { baseApiUrl } from '../global';
import FloodAreas from './flood-areas';

interface FloodAreasWarningsState {
    floodAreas: any;
    floodWarnings: any;
}

export default class FloodAreasWarnings extends React.Component {

    state: FloodAreasWarningsState;

    constructor(props: any) {
        super(props);

        this.state = {
            floodAreas: null,
            floodWarnings: null,
        };
    }

    componentDidMount() {

        this.getFloodAreas();
        //this.getFloodWarnings();

        // this.interval = setInterval(() => {
        //     this.getFloodAreas();
        //     this.getFloodWarnings();
        // }, 3600000);
    }

    componentWillUnmount() {
        // clearInterval(this.interval);
    }

    getFloodAreas = () => {

        fetch(baseApiUrl + '/fa-fw.php?op=fa', {
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
            }
        })
            .then(response => {
                return response.json();
            })
            .then(fa => {
                this.setState({ floodAreas: JSON.parse(fa) });
            });
    }

    getFloodWarnings = () => {

        fetch(baseApiUrl + '/fa-fw.php?op=fw', {
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
            }
        })
            .then(response => {
                return response.json();
            })
            .then(fw => {
                this.setState({ floodWarnings: JSON.parse(fw) });
            });
    }

    render(): JSX.Element {

        return (
            <Row>
                <Col xs={12}>
                    <FloodAreas floodAreas={this.state.floodAreas} />
                </Col>
            </Row>
        );
    }
}
