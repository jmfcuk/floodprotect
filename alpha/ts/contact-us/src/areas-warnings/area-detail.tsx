import * as React from 'react';
import FMap from '../maps/fmap';

interface AreaDetailProps {
    item: any;
}

export default class AreaDetail extends React.Component<AreaDetailProps> {

    render() {

        let content = (<div>No Content</div>);

        if (this.props.item) {
            const lat = this.props.item.lat;
            const long = this.props.item.long;
            content = (
            <FMap center={[lat, long]}
                    processMarker={false}
                    processOverlay={false}
                    deleteMode={false} />
            );
        }

        return (content);
    }
}
