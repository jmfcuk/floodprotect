import * as React from 'react';
import {
    Row,
    Col
} from 'react-bootstrap';
import { toast } from 'react-toastify';
import Overview from '../overview/overview';
import FloodAreasWarnings from '../areas-warnings/flood-areas-warnings';
import ContactForm from '../contact/contact-form';
import { ContactDetails } from '../contact/contact-details.d';
import { ViewType, baseApiUrl, isNullOrWhitespace } from '../global';

interface MainProps {
    currentView: ViewType;
}

export default class Main extends React.Component<MainProps> {

    notify = (msg: string) => toast(msg);

    onSendContact = (data: ContactDetails) => {

        if (!data) {
            return;
        }

        if (!data.fname || isNullOrWhitespace(data.fname)) {
            this.notify('Please enter your first name.');
            return;
        }

        if (!data.email || isNullOrWhitespace(data.email)) {
            this.notify('Please enter your email.');
            return;
        }

        const body = JSON.stringify(data);

        const url: string = baseApiUrl + '/contact-us.php';

        fetch(url, {
            method: 'POST',
            mode: 'cors',
            body: body
        });

        this.notify('Thank you.  We will be in touch soon');
        this.setState({ currentView: ViewType.overview });
    }

    render(): JSX.Element {

        const { currentView } = this.props;

        let content: JSX.Element;

        switch (currentView) {

            case ViewType.overview: {
                content = (
                    <Overview />
                );
                break;
            }
            case ViewType.areasAndWarnings: {
                content = (
                    <FloodAreasWarnings />
                );
                break;
            }
            case ViewType.contactUs: {

                content = (
                    <ContactForm onSend={this.onSendContact} />
                );
                break;
            }

            default: content = (<Row><div>No Content</div></Row>); break;
        }

        return (
            <Row>
                <Col xs={12}>
                    {content}
                </Col>
            </Row>
        );
    }
}
