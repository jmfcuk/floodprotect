import * as React from 'react';
import Main from './main';
import { Grid } from 'react-bootstrap';
import Header from './header';
import Menubar from './menubar';
import { ViewType } from '../global';

interface AppProps { }

interface AppState {
    currentView: ViewType;
}

export default class App extends React.Component<AppProps, AppState> {

    state: AppState;

    constructor(props: AppProps) {

        super(props);

        this.state = {
            currentView: ViewType.overview,
        };
    }

    onViewChange = (vt: ViewType) => {
        this.setState({ currentView: vt });
    }

    render() {
        return (
            <Grid>

                <Header />

                <Menubar
                    currentView={this.state.currentView}
                    onViewChange={this.onViewChange}
                />

                <Main currentView={this.state.currentView} />

                {/* <Header />
                <Row>
                    <Col xs={2} />
                    <Col xs={8}>
                        <Menubar
                            currentView={this.state.currentView}
                            onViewChange={this.onViewChange}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={2} />
                    <Col xs={8}>
                        <Main currentView={this.state.currentView} />
                    </Col>
                </Row> */}
            </Grid>
        );
    }
}
