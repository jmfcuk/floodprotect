import * as React from 'react';
import {
    Row, Col, Button,
    ButtonGroup
} from 'react-bootstrap';
import { ViewType } from '../global';

interface MenubarProps {

    currentView: ViewType;
    onViewChange: any;
}

const Menubar = (props: MenubarProps) => {

    const overviewTitle: string = 'Overview';
    const areasAndWarningsTitle: string = 'Flood Areas and Warnings';
    const contactUsTitle: string = 'Contact Us';

    return (
        <Row>
            <Col xs={8}>
                <ButtonGroup>
                    <Button
                        className="btn"
                        active={props.currentView === ViewType.overview}
                        onClick={e => props.onViewChange(ViewType.overview)}>
                        {overviewTitle}
                    </Button>
                    <Button
                        className="btn"
                        active={props.currentView === ViewType.areasAndWarnings}
                        onClick={e => props.onViewChange(ViewType.areasAndWarnings)}
                    >
                        {areasAndWarningsTitle}
                    </Button>
                    <Button
                        className="btn"
                        active={props.currentView === ViewType.contactUs}
                        onClick={e => props.onViewChange(ViewType.contactUs)}
                    >
                        {contactUsTitle}
                    </Button>
                    <a href="docs/floodprotect-product-info.pdf" download="floodprotect-product-info.pdf">
                        <Button className="btn">Download our Brochure</Button>
                    </a>
                </ButtonGroup>
            </Col>
        </Row>
    );
}

export default Menubar;
