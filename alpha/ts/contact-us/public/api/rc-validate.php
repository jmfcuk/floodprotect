<?php

    require_once "HTTP/Request2.php";

    header("Access-Control-Allow-Origin: *");

	$in = trim(file_get_contents("php://input"));
    $data = json_decode($in);

    $secret = "6LdTc4UUAAAAAI7OHLar_wsQiacVh58_ADSm56ED";
    $url = "https://www.google.com/recaptcha/api/siteverify";
        
    $body = "secret=" . $secret . "&response=" . $data->response;

    $req = new HTTP_Request2($url, HTTP_Request2::METHOD_POST);
    $req->setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
    $req->setBody($body);

    $result = $req->send()->getBody();
    
    echo $result;
?>
