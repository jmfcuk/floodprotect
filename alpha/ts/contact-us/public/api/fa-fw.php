<?php

    require_once "HTTP/Request2.php";

    header("Access-Control-Allow-Origin: *");

    $op = $_GET['op'];

    ini_set("display_errors", 1);
    error_reporting(E_ALL);

    //$areasUrl = "https://environment.data.gov.uk/flood-monitoring/id/floodAreas";
    $areasUrl = "../data/flood-areas.json";
    $warningsUrl = "https://environment.data.gov.uk/flood-monitoring/id/floods";

    if($op == 'fa') {

        $url = $areasUrl;

        $data = file_get_contents($url);

        echo json_encode($data);

    } else if ($op == 'fw') {
        $url = $warningsUrl;
        $request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
		try {
            $response = $request->send();
            $status = $response->getStatus();
			if (200 == $status) {
				echo json_encode($response->getBody());
			} else {
				echo json_encode("{}");
			}
		} catch (HTTP_Request2_Exception $e) {
			echo json_encode("{}");
		}
    }
?>
