<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");

    $from = $_GET['from'];
    $to = $_GET['to'];

    $server = "localhost";
    $db = "test_db";
    $uid = "john";
    $pwd = "john";

    $pdo = new PDO("mysql:host=$server;dbname=$db", $uid, $pwd);

    $sql = "SELECT * FROM contact_us WHERE received BETWEEN :from AND :to;";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(":from", $from);
    $stmt->bindParam(":to", $to);
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
?>
