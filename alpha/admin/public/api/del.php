<?php

    $ids = $_GET["ids"];

    header("Access-Control-Allow-Origin: *");

    $server = "localhost";
    $db = "test_db";
    $uid = "john";
    $pwd = "john";

	$pdo = new PDO("mysql:host=$server;dbname=$db", $uid, $pwd);

    $sql = "DELETE FROM contact_us WHERE id IN ($ids);";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(":ids", $ids);

    $stmt->execute();
?>
