<?php

    $from = $_GET['from'];
    $to = $_GET['to'];

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename='contact-us_$from-$to.csv'");
    header("Pragma: no-cache");
    header("Expires: 0");

    $server = "localhost";
    $db = "test_db";
    $uid = "john";
    $pwd = "john";

	$pdo = new PDO("mysql:host=$server;dbname=$db", $uid, $pwd);

    $sql = "SELECT * FROM contact_us WHERE received BETWEEN :from AND :to;";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(":from", $from);
    $stmt->bindParam(":to", $to);

    $stmt->execute();

    $num_cols = $stmt->columnCount();
    $cols = array();
    for ($i = 0; $i < $num_cols; $i++) {
        $col = $stmt->getColumnMeta($i);
        $cols[] = ucfirst($col["name"]);
    }

    $output = fopen("php://output", "w");
    fputcsv($output, $cols);
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      fputcsv($output, $row);
    }
?>
