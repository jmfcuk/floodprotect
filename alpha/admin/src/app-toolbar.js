import React from 'react';
import {
    Toolbar,
    Tooltip,
    Grid,
    TextField
} from '@material-ui/core';
import baseApiUrl from './const';
var moment = require('moment');

const add1Day = (dt) => {

    const m = moment(dt, 'YYYY-MM-DD');
    let plus1 = m.add(1, 'days').format('YYYY-MM-DD');

    return plus1;
}

export const AppToolbar = props => {

    const {
        startDate, endDate, startDateChanged, endDateChanged
    } = props;

    const ed = add1Day(endDate);
    const csvUrl = baseApiUrl + '/contact-us-csv.php?from=' + startDate + '&to=' + ed;

    return (

        <Toolbar>
            <Grid container align="center">
                <Grid item xs={2}>
                <br/>
                    <Tooltip title="Select start date">
                        <TextField
                            id="startDate"
                            type="date"
                            label="From"
                            onChange={startDateChanged}
                            value={startDate}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Tooltip>
                </Grid>
                <Grid item xs={2}>
                <br/>
                    <Tooltip title="Select end date">
                        <TextField
                            id="endDate"
                            type="date"
                            label="To"
                            onChange={endDateChanged}
                            value={endDate}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Tooltip>
                </Grid>
                <Grid item xs={8}>
                <br />
                    <a href={csvUrl}><h4>Click here to download enquiries from the contact us website</h4></a>
                </Grid>
            </Grid>
        </Toolbar>
    );
}

export default AppToolbar;

