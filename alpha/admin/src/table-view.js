import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const cols = [
    { id: 'received', numeric: true, disablePadding: true, label: 'Received' },
    { id: 'salutation', numeric: true, disablePadding: true, label: 'Salutation' },
    { id: 'fname', numeric: true, disablePadding: true, label: 'First Name' },
    { id: 'lname', numeric: true, disablePadding: true, label: 'Last Name' },
    { id: 'organization', numeric: true, disablePadding: true, label: 'Organisation' },
    { id: 'message', numeric: true, disablePadding: true, label: 'Message' },
    { id: 'email', numeric: true, disablePadding: true, label: 'Email' },
    { id: 'phone', numeric: true, disablePadding: true, label: 'Phone' },
    { id: 'bestTime', numeric: true, disablePadding: true, label: 'Best Time' },
    { id: 'street', numeric: true, disablePadding: true, label: 'Street' },
    { id: 'city', numeric: true, disablePadding: true, label: 'City' },
    { id: 'postcode', numeric: true, disablePadding: true, label: 'Postcode' },
    { id: 'subscribed', numeric: true, disablePadding: true, label: 'Subscribed' },
];

class TableViewHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { onSelectAllClick, order, orderBy, numSelected, rowCount } = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <Checkbox
                            indeterminate={numSelected > 0 && numSelected < rowCount}
                            checked={numSelected === rowCount}
                            onChange={onSelectAllClick}
                        />
                    </TableCell>
                    {cols.map(col => {
                        return (
                            <TableCell
                                key={col.id}
                                sortDirection={orderBy === col.id ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={col.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === col.id}
                                        direction={order}
                                        onClick={this.createSortHandler(col.id)}
                                    >
                                        {col.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

TableViewHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};


let TableViewToolbar = props => {

    const { numSelected, onDelete } = props;

    return (
        <Toolbar>
            <div>
                <Typography color="inherit" variant="subtitle1">
                    {numSelected} selected
          </Typography>
            </div>

            <div>
                {numSelected > 0 ? (
                    <IconButton aria-label="Delete" onClick={onDelete}>
                        <DeleteIcon />
                    </IconButton>
                ) : (
                        null
                    )}
            </div>
        </Toolbar>
    );
};

TableViewToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};

class TableView extends React.Component {

    state = {
        order: 'asc',
        orderBy: 'calories',
        selected: [],
        page: 0,
        rowsPerPage: 5,
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({ order, orderBy });
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(state => ({ selected: this.props.json.map(n => n.id) }));
            return;
        }
        this.setState({ selected: [] });
    };

    handleClick = (event, id) => {

        const { selected } = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({ selected: newSelected });
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    handleDelete = () => {
        if(window.confirm('Delete selected records?')) {
            const { selected } = this.state;
            this.props.onDelete(selected);
            this.setState({ selected: [] });
        }
    }

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    render() {
        const { order, orderBy, selected, rowsPerPage, page } = this.state;
        const { json } = this.props;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, json.length - page * rowsPerPage);

        return (
            <div>
                <TableViewToolbar numSelected={selected.length} onDelete={this.handleDelete} />
                <div style={{'overflow-x': 'auto'}}>
                    <Table aria-labelledby="tableTitle">
                        <TableViewHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={this.handleSelectAllClick}
                            onRequestSort={this.handleRequestSort}
                            rowCount={json.length}
                        />
                        <TableBody>
                            {stableSort(json, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(n => {
                                    const isSelected = this.isSelected(n.id);
                                    return (
                                        <TableRow
                                            hover
                                            onClick={event => this.handleClick(event, n.id)}
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            id={n.id}
                                            key={n.id}
                                            selected={isSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox checked={isSelected} />
                                            </TableCell>
                                            <TableCell component="th" scope="row" padding="none">
                                                {n.received}
                                            </TableCell>
                                            <TableCell>{n.salutation}</TableCell>
                                            <TableCell>{n.fname}</TableCell>
                                            <TableCell>{n.lname}</TableCell>
                                            <TableCell>{n.organization}</TableCell>
                                            <TableCell>{n.message}</TableCell>
                                            <TableCell>{n.email}</TableCell>
                                            <TableCell>{n.phone}</TableCell>
                                            <TableCell>{n.bestTime}</TableCell>
                                            <TableCell>{n.street}</TableCell>
                                            <TableCell>{n.city}</TableCell>
                                            <TableCell>{n.postcode}</TableCell>
                                            <TableCell>{n.subscribed}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={json.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </div>
        );
    }
}

TableView.propTypes = {
    json: PropTypes.array.isRequired,
};

export default TableView;



