import React from 'react';
import {
    AppBar,
    Toolbar,
    Typography,
    Grid
} from '@material-ui/core';

export const Header = props => {

    return (
        <AppBar position="static" color="default">
            <Toolbar>
                <Grid container>
                    <Grid item xs={6}>
                        <Typography variant="title" color="inherit">
                            Contact Us Enquiries
                </Typography>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}

export default Header;
