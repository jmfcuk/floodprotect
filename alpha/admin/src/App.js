import React, { Component } from 'react';
import { Grid, Card } from '@material-ui/core';
import baseApiUrl from './const';
import TableView from './table-view';
import Header from './header';
import AppToolbar from './app-toolbar';
var moment = require('moment');

class App extends Component {

    constructor(props) {

        super(props);

        const end = moment().format('YYYY-MM-DD');
        const start = moment().subtract(1, 'weeks').format('YYYY-MM-DD');

        this.state = {
            json: [],
            startDate: start,
            endDate: end
        };
    }

    componentDidMount() {

        this.getContactUsJson();
    }

    add1Day = (dt) => {

        const m = moment(dt, 'YYYY-MM-DD');
        let plus1 = m.add(1, 'days').format('YYYY-MM-DD');

        return plus1;
    }

    getContactUsJson = () => {
        const { startDate } = this.state;
        const { endDate } = this.state;

        const ed = this.add1Day(endDate);

        const url = baseApiUrl + '/contact-us-json.php?from=' + startDate + '&to=' + ed;

        fetch(url)
            .then((resp) => { return resp.json(); })
            .then((json) => {
                this.setState({ json: json });
            }).catch((reason) => this.setState({ json: [] }));
    }

    startDateChanged = (e) => {

        const sd = e.target.value;

        this.setState({ startDate: sd }, () => {
            this.getContactUsJson();
        });
    }

    endDateChanged = (e) => {

        const ed = e.target.value;

        this.setState({ endDate: ed }, () => {
            this.getContactUsJson();
        });
    }

    onDelete = selected => {
        
        const ids = selected.join(',');
        const url = baseApiUrl + '/del.php?ids=' + ids;

        fetch(url)
            .then((resp) => { 
                this.getContactUsJson();
            });
    }

    render() {

        const { json } = this.state;

        const { startDate } = this.state;
        const { endDate } = this.state;

        return (
            <div>
                <Header />
                <AppToolbar
                    startDate={startDate}
                    endDate={endDate}
                    startDateChanged={this.startDateChanged}
                    endDateChanged={this.endDateChanged}
                />
                <Grid container>
                    <Grid item xs={12}>
                        <Card>
                            <TableView json={json} onDelete={this.onDelete} />
                            {/* {JSON.stringify(json)} */}
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default App;
