<?php

    header("Access-Control-Allow-Origin: https://floodprotect.co/beta");
    //header("Access-Control-Allow-Origin: *");

function insert_db($data, $received, $ipFrom) {

    // $server = "localhost";
    // $db = "test_db";
    // $uid = "john";
    // $pwd = "john";

    $server = "localhost:3306";
    $db = "reabourn_floodprotect_beta";
    $uid = "reabourn_fp-db";
    $pwd = "fpdbpwd123!";
    
    $pdo = new PDO("mysql:host=$server;dbname=$db", $uid, $pwd);
    $salutation = $data->salutation;
    $fname = $data->fname;
    $lname = $data->lname;
    $organization = $data->organization;
    $email = $data->email;
    $phone = $data->phone;
    $bestTime = $data->bestTime;
    $street1 = $data->street1;
    $street2 = $data->street2;
    $city = $data->city;
    $postcode = $data->postcode;
    $message = $data->message;

    $sub = 0;
    if($data->subscribed === true) {
        $sub = 1;
    }
        
    $sql = "INSERT INTO contact_us (received, ipFrom, salutation, fname, lname, organization, email, phone, bestTime, ";
    $sql .= "street1, street2, city, postcode, message, subscribed) ";
    $sql .= "VALUES (:received, :ipFrom, :salutation, :fname, :lname, :organization, :email, :phone, :bestTime, ";
    $sql .= ":street1, :street2, :city, :postcode, :message, :subscribed)";
        
    $stmt = $pdo->prepare($sql);
    
    $stmt->bindParam(':received', $received);
    $stmt->bindParam(':ipFrom', $ipFrom);
    $stmt->bindParam(':salutation', $salutation);
    $stmt->bindParam(':fname', $fname);
    $stmt->bindParam(':lname', $lname);
    $stmt->bindParam(':organization', $organization);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':bestTime', $bestTime);
    $stmt->bindParam(':street1', $street1);
    $stmt->bindParam(':street2', $street2);
    $stmt->bindParam(':city', $city);
    $stmt->bindParam(':postcode', $postcode);
    $stmt->bindParam(':message', $message);
    $stmt->bindParam(':subscribed', $sub);

    $stmt->execute();
}

function buildNotification($data, $received) {

    $salutation = $data->salutation;
    $fname = $data->fname;
    $lname = $data->lname;
    $organization = $data->organization;
    $email = $data->email;
    $phone = $data->phone;
    $bestTime = $data->bestTime;
    $street1 = $data->street1;
    $street2 = $data->street2;
    $city = $data->city;
    $postcode = $data->postcode;
    $subscribed = $data->subscribed;
    $message = $data->message;

    $notification = PHP_EOL;
    $notification .= "Received:\t\t" . $received . PHP_EOL;
    $notification .= "Salutation:\t\t" . $salutation . PHP_EOL;
    $notification .= "Firstname:\t\t" .$fname . PHP_EOL;
    $notification .= "Lastname:\t\t" .$lname . PHP_EOL;
    $notification .= "Organisation\t\t" .$organization . PHP_EOL;
    $notification  .= "Message:\t\t" . $message . PHP_EOL;
    $notification .= "Email:\t\t" . $email . PHP_EOL;
    $notification .= "Phone:\t\t" . $phone . PHP_EOL;
    $notification .= "Best Time:\t\t" . $bestTime . PHP_EOL;
    $notification .= "Street1:\t\t" . $street1 . PHP_EOL;
    $notification .= "Street2:\t\t" . $street2 . PHP_EOL;
    $notification .= "City / Town:\t\t" . $city . PHP_EOL;
    $notification .= "Postcode:\t\t" . $postcode . PHP_EOL;
    $notification .= "Subscribed:\t\t" . $subscribed . PHP_EOL;
    
    return $notification;
}

function sendAck($name, $email) {

    $from = "info@floodprotect.co";
    $to = $email;
    $subject = "Your Floodprotect Enquiry";

    $html = file_get_contents("enquiry-ack-template.html");

    $message = $html;

    $message = str_replace("[{{name}}]", $name, $message);
    $message = str_replace("[{{line1}}]", "Thank you very much for your interest in Floodprotect", $message);
    $message = str_replace("[{{line2}}]", "  A representative will be in touch with you shortly", $message);
    $message = str_replace("[{{line3}}]", "Best Regards", $message);
    $message = str_replace("[{{line4}}]", "The Floodprotect Team", $message);

    $headers = "From: $from\r\n";
    $headers .= "Reply-To: $from\r\n";
    $headers .= "Return-Path: $from\r\n";
    $headers .= "Content-Type: text/html; charset=iso-8859-1\r\n";
    $headers .= "X-Mailer: PHP\r\n";

    mail($to, $subject, $message, $headers);
}

function sendNotification($notification) {

    $from = "noreply@floodprotect.co"; 
    $to = "johnfield@floodprotect.co";
    $subject = "Floodprotect Enquiry";
    $message = $notification;

    $headers = "From: $from\r\n";
    $headers .= "Reply-To: $from\r\n";
    $headers .= "Return-Path: $from\r\n";
    $headers .= "X-Mailer: PHP\r\n";

    mail($to, $subject, $message, $headers);
}

  $body = trim(file_get_contents("php://input"));

  $data = json_decode($body);
  $dt = date("Y-m-d H:i:s");
  
  $ipFrom = $_SERVER["REMOTE_ADDR"];

  try {
    insert_db($data, $dt, $ipFrom);
  } catch(Exception $e) {

  }

  $notification = buildNotification($data, $dt);

  sendNotification($notification);

  sendAck($data->fname, $data->email);
?>
