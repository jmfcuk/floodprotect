<?php

    header("Access-Control-Allow-Origin: https://floodprotect.co/beta");
    //header("Access-Control-Allow-Origin: *");
    
    ini_set("include_path", '/home/reabourn/php:' . ini_get("include_path") );
    
    require_once "HTTP/Request2.php";

    $op = $_GET['op'];

    //$areasUrl = "https://environment.data.gov.uk/flood-monitoring/id/floodAreas";
    $areasUrl = "../data/flood-areas.json";
    $warningsUrl = "https://environment.data.gov.uk/flood-monitoring/id/floods";
    //$warningsUrl = "../data/flood-warnings.json";

    if($op == 'fa') {

        $url = $areasUrl;

        $data = file_get_contents($url);

        echo json_encode($data);

    } else if ($op == 'fw') {

        $url = $warningsUrl;

        // $data = file_get_contents($url);

        // echo json_encode($data);

        $request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
        
		try {
            $response = $request->send();
            $status = $response->getStatus();
			if (200 == $status) {
				echo json_encode($response->getBody());
			} else {
				echo json_encode("{}");
			}
		} catch (HTTP_Request2_Exception $e) {
			echo json_encode("{}");
		}
    }
?>
