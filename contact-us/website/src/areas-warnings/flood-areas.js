import React from 'react';
import {
    Grid,
    Row,
    Col,
    ListGroup,
    ListGroupItem,
} from 'react-bootstrap';
import './flood-areas.css';
import Filter from './filter';
import AreaDetail from './area-detail';
import SubHeading from '../app/sub-heading';
const uniqueid = require('uniqid');

export default class FloodAreas extends React.Component {

    state = {
        counties: [],
        selectedCounty: '',
        filteredCounties: [],
        countyAreas: [],
        filteredCountyAreas: [],
        selectedCountyArea: null
    };

    componentDidUpdate(prevProps) {

        if (this.props.floodAreas !== prevProps.floodAreas) {
            this.getCounties();
        }
    }

    getCounties = () => {

        const { floodAreas } = this.props;

        fetch('data/uk-counties.json')
            .then(response => {
                return response.json();
            })
            .then(counties => {

                const relevant = counties.filter((c) => {
                    let b = false;
                    for (let fa of floodAreas.items) {
                        const itemCounty = fa.county.toLowerCase();
                        const county = c.toLowerCase();
                        if (itemCounty.includes(county)) {
                            b = true;
                            break;
                        }
                    }

                    return b;
                }).sort();

                this.setState({
                    counties: relevant,
                    filteredCounties: relevant
                });
            });
    }

    onCountyFilterChange = (e) => {

        const mask = e.target.value.toLowerCase();

        const { counties } = this.state;

        const fc = counties.filter((c) => {
            if (c.toLowerCase().includes(mask)) {
                return true;
            }
            return false;
        });

        this.setState({ filteredCounties: fc });
    }

    onSelectCounty = (c) => {

        let items = [];

        if (this.props.floodAreas) {

            items = this.props.floodAreas.items.filter((item) => {
                return item.county.toLowerCase().includes(c.toLowerCase());

            });
        }

        this.setState({
            selectedCounty: c,
            countyAreas: items,
            filteredCountyAreas: items,
            selectedCountyArea: null
        });
    }

    onCountyAreaFilterChange = (e) => {

        const mask = e.target.value.toLowerCase();

        const { countyAreas } = this.state;

        const fca = countyAreas.filter((ca) => {
            if (ca.description.toLowerCase().includes(mask)) {
                return true;
            }
            return false;
        });

        this.setState({ filteredCountyAreas: fca });
    }

    onSelectCountyArea = (ca) => {

        this.setState({ selectedCountyArea: ca });
    }

    render() {

        const { selectedCounty, selectedCountyArea } = this.state;

        let content = null;

        if (selectedCountyArea) {
            content = (<AreaDetail item={selectedCountyArea} />);
        } else {
            content = (

                <Row>
                    <Col xs={12}>
                        <Filter
                            label={'Areas (' + this.state.filteredCountyAreas.length + ')'}
                            onFilterChange={this.onCountyAreaFilterChange} />
                        <ListGroup>
                            {
                                this.state.filteredCountyAreas &&
                                this.state.filteredCountyAreas.map((ca) => {
                                    return (
                                        <ListGroupItem
                                            key={uniqueid()}
                                            onClick={e => { this.onSelectCountyArea(ca) }}>
                                            {ca.description}
                                        </ListGroupItem>
                                    )
                                })
                            }
                        </ListGroup>
                    </Col>
                </Row>
            )
        }

        return (

            <React.Fragment>

                <SubHeading
                    title="UK Flood Areas"
                    blurb={<Row>Flood areas are defined by the
                        <a
                            href="https://www.gov.uk/government/organisations/environment-agency"
                            target="_blank" rel="noopener noreferrer"> Environment Agency </a> as
                        <i>"geographic regions to which a given flood alert or warning may apply"</i>
                    </Row>}
                    footer={<Row>This uses Environment Agency flood and river level data from
                        the real-time data API (Beta).
                        See <a
                            href="https://environment.data.gov.uk/"
                            target="_blank"
                            rel="noopener noreferrer">here</a> for more details<br /><br/><br/></Row>}
                            showImage
                />

                <Row>
                    <Col xs={4}>
                        <Filter
                            label={'Counties (' + this.state.filteredCounties.length + ')'}
                            onFilterChange={this.onCountyFilterChange} />
                        <ListGroup>
                            {
                                this.state.filteredCounties &&
                                this.state.filteredCounties.map((c) => {
                                    return (
                                        <ListGroupItem
                                            key={uniqueid()}
                                            active={c === selectedCounty}
                                            onClick={e => this.onSelectCounty(c)}
                                        >
                                            {c}
                                        </ListGroupItem>
                                    )
                                })
                            }
                        </ListGroup>
                    </Col>
                    <Col xs={8}>
                        {content}
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}
