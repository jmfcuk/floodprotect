import React from 'react';
import { Row, Col } from 'react-bootstrap';
import FMap from '../maps/fmap';

const AreaDetail = props => {

    let content = (<div>No Content</div>);

    if (props.item) {

        const lat = props.item.lat;
        const long = props.item.long;

        content = (
            <React.Fragment>
                <Row className="text-center">
                    <h3>{props.item.label}</h3>
                </Row>
                <Row className="text-center">
                    <FMap center={[lat, long]}
                        processMarker={false}
                        processOverlay={false}
                        deleteMode={false} />
                    <br />
                </Row>
                <Row className="text-center">
                    <h4>{props.item.description}</h4>
                </Row>
                 {/* {JSON.stringify(props.item)} */}
            </React.Fragment>
        );
    }

    return (content);
}

export default AreaDetail;