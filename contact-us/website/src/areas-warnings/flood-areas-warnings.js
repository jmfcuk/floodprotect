import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { baseApiUrl } from '../global';
import FloodAreas from './flood-areas';

export default class FloodAreasWarnings extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            floodAreas: null,
        };
    }

    componentDidMount() {

        this.getFloodAreas();
    }

    getFloodAreas = () => {

        fetch(baseApiUrl + '/fa-fw.php?op=fa', {
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
            }
        })
            .then(response => {
                return response.json();
            })
            .then(fa => {
                this.setState({ floodAreas: JSON.parse(fa) });
            });
    }

    render() {

        return (
            <Row>
                <Col xs={12}>
                    <FloodAreas floodAreas={this.state.floodAreas} />
                </Col>
            </Row>
        );
    }
}
