export const baseApiUrl = 'https://floodprotect.co/beta/api';
//export const baseApiUrl = 'http://localhost:9000';

export const mainBackgroundImage = 'img/main-background.jpg';

export const ViewType = {
    overview: 0,
    floodAreas: 1,
    contactUs: 2
}

export const isNullOrWhitespace = (str) => {

    return !str || !str.trim();
}

