import React from 'react';
import {
    Form,
    FormGroup,
    ControlLabel,
    FormControl,
    Row,
    Col,
    Modal,
    Button
} from 'react-bootstrap';
import { toast } from 'react-toastify';
import Recaptcha from 'react-recaptcha';
import './contact-us.css';
import PrivacyNotice from './privacy-notice';
import SubHeading from '../app/sub-heading';
import { ViewType, baseApiUrl, isNullOrWhitespace } from '../global';

class ContactUs extends React.Component {

    notify = msg => toast(msg);

    constructor(props, context) {
        super(props, context);

        this.state = {
            contactDetails: {
                salutation: '',
                fname: '',
                lname: '',
                organization: '',
                email: '',
                phone: '',
                bestTime: 'never',
                street1: '',
                street2: '',
                city: '',
                postcode: '',
                subscribed: false,
                message: ''
            },
            captchaVerified: false,
            showPrivacy: false
        };
    }

    salutationChange = (e) => {

        let cd = this.state.contactDetails;
        cd.salutation = e.target.value;

        this.setState({ contactDetails: cd });
    }

    fnameChange = (e) => {

        let cd = this.state.contactDetails;
        cd.fname = e.target.value;

        this.setState({ contactDetails: cd });
    }

    lnameChange = (e) => {

        let cd = this.state.contactDetails;
        cd.lname = e.target.value;

        this.setState({ contactDetails: cd });
    }

    organizationChange = (e) => {

        let cd = this.state.contactDetails;
        cd.organization = e.target.value;

        this.setState({ contactDetails: cd });
    }

    emailChange = (e) => {

        let cd = this.state.contactDetails;
        cd.email = e.target.value;

        this.setState({ contactDetails: cd });
    }

    phoneChange = (e) => {

        let cd = this.state.contactDetails;
        cd.phone = e.target.value;

        this.setState({ contactDetails: cd });
    }

    bestTimeChange = (e) => {

        let cd = this.state.contactDetails;
        cd.bestTime = e.target.value;

        this.setState({ contactDetails: cd });
    }

    street1Change = (e) => {

        let cd = this.state.contactDetails;
        cd.street1 = e.target.value;

        this.setState({ contactDetails: cd });
    }

    street2Change = (e) => {

        let cd = this.state.contactDetails;
        cd.street2 = e.target.value;

        this.setState({ contactDetails: cd });
    }

    cityChange = (e) => {

        let cd = this.state.contactDetails;
        cd.city = e.target.value;

        this.setState({ contactDetails: cd });
    }

    postcodeChange = (e) => {

        let cd = this.state.contactDetails;
        cd.postcode = e.target.value;

        this.setState({ contactDetails: cd });
    }

    messageChange = (e) => {

        let cd = this.state.contactDetails;
        cd.message = e.target.value;

        this.setState({ contactDetails: cd });
    }

    subscribedChange = (e) => {

        let cd = this.state.contactDetails;
        cd.subscribed = e.target.checked;

        this.setState({ contactDetails: cd });
    }

    isNullOrWhitespace = (str) => {

        return !str || !str.trim();
    }

    onRecaptchaLoad = () => {

    }

    onRecaptchaVerify = (value) => {

        const url = baseApiUrl + '/rc-validate.php';

        const data = {
            response: value
        };

        const body = JSON.stringify(data);

        fetch(url, {
            method: 'POST',
            mode: 'cors',
            body: body
        })
            .then(response => {
                return response.json();
            })
            .then(json => {
                if (json.success) {
                    this.setState({ captchaVerified: true });
                }
            });
    }

    onSend = () => {

        const data = this.state.contactDetails;

        this.onSendContact(data);
    }

    onSendContact = (data) => {

        if (!data) {
            return;
        }

        if (!data.fname || isNullOrWhitespace(data.fname)) {
            this.notify('Please enter your first name.');
            return;
        }

        if (!data.email || isNullOrWhitespace(data.email)) {
            this.notify('Please enter your email.');
            return;
        }

        const body = JSON.stringify(data);

        const url = baseApiUrl + '/contact-us.php';

        fetch(url, {
            method: 'POST',
            mode: 'cors',
            body: body
        });

        this.notify('Thank you.  We will be in touch soon');

        this.props.onViewChange(ViewType.overview);
    }

    togglePrivacy = () => {

        const { showPrivacy } = this.state;

        this.setState({ showPrivacy: !showPrivacy });
    }

    render() {

        return (
            <React.Fragment>

                <SubHeading
                    title="Contact Us"
                    blurb={<div><Row>Fill out your details below and we will get back to you.</Row>
                        <Row>Only a first name and email are required</Row></div>}
                    showImage
                />

                <Form>
                    <Row>
                        <Col xs={2}>
                            <FormGroup>
                                <ControlLabel>Title</ControlLabel>
                                <select
                                    id="salutation"
                                    className="form-control"
                                    onChange={this.salutationChange}
                                >
                                    <option value="">Title</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                    <option value="Sir">Sir</option>
                                    <option value="Rev">Rev</option>
                                </select>
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={5}>
                            <FormGroup>
                                <ControlLabel>First Name <strong style={{ color: 'red' }}>*</strong></ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="First Name (required)"
                                    onChange={this.fnameChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={5}>
                            <FormGroup>
                                <ControlLabel>Last Name</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="Last Name"
                                    onChange={this.lnameChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12}>
                            <FormGroup>
                                <ControlLabel>Organisation</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="Organisation"
                                    onChange={this.organizationChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={4}>
                            <FormGroup>
                                <ControlLabel>
                                    Email <strong style={{ color: 'red' }}>*</strong>
                                </ControlLabel>
                                <FormControl
                                    type="email"
                                    placeholder="Email (required)"
                                    onChange={this.emailChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={4}>
                            <FormGroup>
                                <ControlLabel>Phone</ControlLabel>
                                <FormControl
                                    type="tel"
                                    placeholder="Phone"
                                    onChange={this.phoneChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={4}>
                            <div className="pull-right">
                                <label htmlFor="bestTime">Best Time to call</label>
                                <select
                                    className="form-control"
                                    onChange={this.bestTimeChange}>
                                    <option value="never">Never</option>
                                    <option value="any (09:00 - 20:00)">Any Time (09:00 - 20:00)</option>
                                    <option value="am (09:00 - 12:00)">Morning (09:00 - 12:00)</option>
                                    <option value="pm (12:00 - 17:00">Afternoon (12:00 - 17:00)</option>
                                    <option value="eve (17:00 - 20:00)">Evening (17:00 - 20:00)</option>
                                </select>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={6}>
                            <FormGroup>
                                <ControlLabel>Street 1</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="Street 1"
                                    onChange={this.street1Change}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={6}>
                            <FormGroup>
                                <ControlLabel>Street 2</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="Street 2"
                                    onChange={this.street2Change}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={6}>
                            <FormGroup>
                                <ControlLabel>City</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="City"
                                    onChange={this.cityChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={6}>
                            <FormGroup>
                                <ControlLabel>Postcode</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="Postcode"
                                    onChange={this.postcodeChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <FormGroup>
                                <ControlLabel>Message</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="Anything you want to tell or ask us?"
                                    onChange={this.messageChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row className="text-center">
                        <Col xs={12} md={4}>
                            <FormGroup>
                                <ControlLabel>
                                    Subscribe for Updates
                                </ControlLabel>
                                <FormControl
                                    type="checkbox"
                                    onChange={this.subscribedChange}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </Col>
                        <Col xs={12} md={4}>
                            <Recaptcha
                                sitekey="6LdTc4UUAAAAAAvEvwvT0OuSMX2laK3sK4Dr9FgI"
                                verifyCallback={this.onRecaptchaVerify}
                                size="normal"
                            />
                        </Col>
                        <Col xs={12} md={4}>
                            <div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <button
                                type="button"
                                className="btn btn-primary"
                                style={{ width: '75%', height: '100%' }}
                                onClick={this.onSend}
                                disabled={this.isNullOrWhitespace(this.state.contactDetails.fname) ||
                                    this.isNullOrWhitespace(this.state.contactDetails.email) ||
                                    !this.state.captchaVerified}
                            >
                                Send
                            </button>
                            <div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            {/* <div>
                                <i>We will be in contact soon</i>
                            </div> */}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <div>
                                By providing your contact information, you are confirming you are an adult 18 years or
                                older and you authorize Floodprotect to contact you by email or telephone with information
                                about Floodprotect products, events, and updates.
                                You may unsubscribe at any time by clicking the link provided in our communications.
          Please review our
                  <a style={{ cursor: 'pointer' }}
                                    onClick={this.togglePrivacy}> Privacy Notice</a> for more information.
              </div>
                        </Col>
                    </Row>
                </Form>

                <Modal show={this.state.showPrivacy} onHide={this.togglePrivacy}>
                    <Modal.Header closeButton={true}>
                        <Modal.Title>Privacy Notice</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <PrivacyNotice />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.togglePrivacy}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment >
        );
    }
}

export default ContactUs;
