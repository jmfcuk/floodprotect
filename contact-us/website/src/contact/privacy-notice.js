import React from 'react';

export const PrivacyNotice = () => {

    return (

                                    <div>
                                        <b>Introduction</b>
                                        <br />Floodprotect is committed to protecting and respecting any
                                        personal information you share with us. This statement describes
                                        what types of information we collect from you, how it is used by
                                        us, how you can manage the information we hold and how you can
            contact us.<br />
                                        <br />Sometimes, the contents of this statement may change so you
                                        may wish to check this page occasionally to ensure you are still
                                        happy to share your information with us. Where possible, we will
            also contact you directly to notify you of these changes.<br />
                                        <br />
                                        <b>What information do we collect?</b>
                                        <br />We only collect information that is necessary, relevant and
            adequate for the purpose you are providing it for.<br />
                                        <br />
                                        <b>Personal data you share with us</b>
                                        <br />We may ask for personal data when you order from us; this
                                        includes when you order from the Website or over the phone, or
            when you sign up to our mailing list.<br />
                                        <br />The information we collect includes but is not limited to:<br />
                                        <br />Name (including title);<br />Address (optional);<br />Phone
            number (optional);<br />Email address.<br />
                                        <br />
                                        <b>Personal data we collect about you</b>
                                        <br />We will collect your information when you use Floodprotect's
                                        websites, applications and our customer service centres. Some of
                                        this data does not identify you personally but provides us with
                                        information about how you use our services and engage with us in
            order to improve our services and make them more useful to you.<br />
                                        <br />The information we collect includes but is not limited to:<br />
                                        <br />The date and time you used our services;<br />Your IP
            address;<br />The internet browser and devices you are using;<br />Any
            information within correspondence you send to us;<br />Where you
                                                                                                                                        engage with us in a business context, we may collect your job
                                                                                                                                        title, company contact details (including email addresses), and
                                                                                                                                        company details (some of which we may obtain from an online or
            public business directories).<br />
                                        <br />
                                        <b>How do we use this information?</b>
                                        <br />Floodprotect will only process information that is necessary
                                        for the purpose for which it has been collected. You will always
                                        have the option not to receive marketing communications from us
                                        (and you can withdraw your consent or object at any time). We will
                                        never send you unsolicited email or communications, or share your
            personal information with anyone else who might.<br />
                                        <br />There are various ways in which we may use or process your
            personal information. We list these below:<br />
                                        <br />
                                        <b>Consent</b>
                                        <br />Where you have provided your consent, we may use and process
                                        your information to contact you from time to time through
                                        electronic channels such as email about promotions, events,
                                        products, services or information which we think may be of
            interest to you.<br />
                                        <br />You can withdraw your consent at any time by emailing{' '}
                                        <a href="mailto:info@floodprotect.co">info@floodprotect.co</a> or,
                                        in relation to any marketing messages you receive, by selecting
            the unsubscribe option included in those messages.<br />
                                        <br />
                                        <b>Contractual performance</b>
                                        <br />We may use and process your personal information where this
                                        is necessary to perform a contract such as to fulfil and complete
            any orders and purchases.<br />
                                        <br />
                                        <br />
                                        <b>Legal obligation</b>
                                        <br />We may process your personal information to comply with any
            legal and/or regulatory requirements.<br />
                                        <br />
                                        <b>Vital interest</b>
                                        <br />We may need to process your personal information to contact
                                        you if there is an urgent safety or product recall notice and we
            need to tell you about it.<br />
                                        <br />
                                        <b>Legitimate interests</b>
                                        <br />We may use and process your personal information as set out
                                        below where it is necessary for us to carry out activities for
            which it is in our legitimate interests as a business to do so.<br />
                                        <br />Processing necessary for us to support customers with sales
            and other enquiries<br />
                                        <br />In order to support sales and/or queries, we may use your
            information:<br />
                                        <br />To respond to correspondence you send to us and fulfil the
            requests you make to us.<br />
                                        <br />To develop and improve our relationship with you, we may use
            your information:<br />
                                        <br />To analyse, evaluate and improve our products and services,
            including use of our website, applications, customer service.<br />
                                        <br />To undertake market analysis and research (including
                                        contacting you with customer surveys) so that we can better
            understand you as a customer.<br />
                                        <br />To provide tailored offers, products and services by post
                                        that we think you will be interested in. We will only send post or
                                        email marketing communications to you if you have provided your
            consent for us to do so.<br />
                                        <br />Processing necessary for us to operate the administrative
            and technical aspects of our business efficiently and effectively.<br />
                                        <br />To protect your personal information, we may use your data:<br />To
                                        verify the accuracy of information that we hold about you and
            create a better understanding of you as a customer;<br />For
                                                                                                                                        network and information security purposes i.e. in order for us to
                                                                                                                                        take steps to protect your information against loss, damage, theft
            or unauthorised access;<br />To comply with a request from you in
                                                                                                                                        connection with the exercise of your rights (for example where you
                                                                                                                                        have asked us not to contact you for marketing purposes, we will
                                                                                                                                        keep a record of this on our suppression lists in order to be able
            to comply with your request);<br />To inform you of updates to our
            terms and conditions and policies.<br />Processing necessary for
                                                                                                                                        us to promote our business, brands and products and measure the
            reach and effectiveness of our campaigns<br />To support and
            promote the Group, we may use your information:<br />To send you
                                                                                                                                        marketing information from time to time after you have purchased a
            product or service from us, or made a purchasing enquiry.<br />To
                                                                                                                                        identify and record when you have received, opened or engaged with
            our website or electronic communications.<br />
                                        <br />How long do we keep your information for?<br />We will not
                                        hold your personal information in an identifiable format for any
                                        longer than is necessary. If you are a customer or otherwise have
                                        a relationship with us we will hold personal information about you
                                        for a longer period than if we have obtained your details in
            connection with a prospective relationship.<br />
                                        <br />How can you manage the information we hold about you?<br />You
                                        have the right as an individual to access any personal information
                                        we hold about you and make corrections if necessary. You also have
                                        the right to withdraw any consent you have previously given us
                                        and, in certain circumstances, ask us to erase information we hold
                                        about you. You can also object to us using your personal
                                        information (where we rely on our business interests to process
            and use your personal information).<br />
                                        <br />You have a number of rights in relation to your personal
                                        information under data protection law. In relation to most rights,
                                        we will ask you for information to confirm your identity and,
                                        where applicable, to help us search for your personal information.
                                        We will respond to you within a month after we have received any
            request (including any identification documents requested).<br />
                                        <br />
                                        <b>You have the right to:</b>
                                        <br />
                                        <br />Ask for a copy of the information that we hold about you;<br />Correct
            and update your information;<br />Withdraw your consent (where we
            rely on it);<br />Object to our use of your information (where we
                                                                                                                                        rely on our legitimate interests to use your personal information)
                                                                                                                                        provided we do not have any lawful reason to continue to use and
                                                                                                                                        process the information. When we do rely on our legitimate
                                                                                                                                        interests to use your personal information for direct marketing,
            we will always comply with your right to object;<br />Erase your
                                                                                                                                        information (or restrict the use of it), provided we do not have
            any lawful reason to continue to use and process that information;<br />Provide
                                                                                                                                        you your information in a structured data file, where we rely on
            your consent to use and process your personal information.<br />
                                        <br />
                                        <br />This version of our Privacy Notice is live from 30 December
                                        2018.
            </div>
    );
}

export default PrivacyNotice;
