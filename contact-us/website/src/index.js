import 'babel-polyfill';
import "isomorphic-fetch";
import "es6-promise";

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/app';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

