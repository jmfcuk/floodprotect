import React from 'react';
import {
    Row
} from 'react-bootstrap';
import SubHeading from '../app/sub-heading';
import OverviewSet from './overview-set';

export default class Overview extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            items: []
        };
    }

    componentDidMount() {
        this.getItems();
    }

    getItems = (reset = false) => {

        const url = 'data/items.json';

        fetch(url)
            .then(response => {
                return response.json();
            })
            .then(its => {
                this.setState({ items: its });
            });
    }

    render() {
        return (
            <React.Fragment>
                <SubHeading
                    title="Overview"
                    blurb={<div>
                        <br />
                        <br />
                        <Row>Read our brochure below and download from the link above</Row>
                        <br />
                        <br />
                    </div>}
                    footer={<Row>
                        <br/><br/>
                        </Row>}
                    showImage
                />
                <OverviewSet
                    items={this.state.items}
                    onPageSelect={this.onPageSelect}
                />
            </React.Fragment>
        );
    }
}
