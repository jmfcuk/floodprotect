import React from 'react';
import {
    Carousel,
    CarouselItem
} from 'react-bootstrap';

const OverviewSet = (props) => {

    const { items, onPageSelect } = props;

    return (
        <Carousel
            slide={true}
            indicators={false}
            interval={0}
            onSelect={onPageSelect}
        >
            {items && items.map((item) => (
                <CarouselItem
                    key={item.id}
                >
                    <img className="img-responsive center-block" src={item.imageUri} />
                </CarouselItem>
            ))}
        </Carousel>
    );
};

export default OverviewSet;
