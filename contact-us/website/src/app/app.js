import React from 'react';
import Main from './main';
import { Grid } from 'react-bootstrap';
//import Header from './header';
import Menubar from './menubar';
import FloodWarningsTicker from './flood-warnings-ticker';
import { ViewType, mainBackgroundImage } from '../global';
import './app.css';

const mainStyle = {
    backgroundImage: 'url(' + mainBackgroundImage + ')'//,
}

export default class App extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            currentView: ViewType.overview,
        };
    }

    onViewChange = (vt) => {
        this.setState({ currentView: vt });
    }

    render() {

        const msg = 'Test';

        return (
            <React.Fragment>
                <Grid fluid style={mainStyle}>
                    <Menubar
                        currentView={this.state.currentView}
                        onViewChange={this.onViewChange}
                    />
                    {/* <Header /> */}
                    <Main currentView={this.state.currentView}
                          onViewChange={this.onViewChange} />
                    <FloodWarningsTicker />

                </Grid>
            </React.Fragment>
        );
    }
}

