import React from 'react';
import {
    Row,
    Col
} from 'react-bootstrap';
//import { toast } from 'react-toastify';
import Overview from '../overview/overview';
import FloodAreasWarnings from '../areas-warnings/flood-areas-warnings';
import ContactUs from '../contact/contact-us';
import { ViewType } from '../global';

export default class Main extends React.Component {

    //notify = msg => toast(msg);

    // onSendContact = (data) => {

    //     if (!data) {
    //         return;
    //     }

    //     if (!data.fname || isNullOrWhitespace(data.fname)) {
    //         this.notify('Please enter your first name.');
    //         return;
    //     }

    //     if (!data.email || isNullOrWhitespace(data.email)) {
    //         this.notify('Please enter your email.');
    //         return;
    //     }


    render() {

        const { currentView } = this.props;

        let content = null;

        switch (currentView) {

            case ViewType.overview: {
                content = (
                    <Overview />
                );
                break;
            }
            case ViewType.floodAreas: {
                content = (
                    <FloodAreasWarnings />
                );
                break;
            }
            case ViewType.contactUs: {

                content = (
                    <ContactUs 
                        onSend={this.onSendContact}
                        onViewChange={this.props.onViewChange} 
                    />
                );
                break;
            }

            default: content = (<Row><div>No Content</div></Row>); break;
        }

        return (
            <Row>
                <Col xs={12}>
                    {content}
                </Col>
            </Row>
        );
    }
}
