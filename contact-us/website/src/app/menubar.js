import React from 'react';
import {
    Navbar,
    Nav,
    NavItem,
    Col
} from 'react-bootstrap';
import { ToastContainer } from 'react-toastify';
import { ViewType } from '../global';

export const Menubar = props => {

    const overviewTitle = 'Overview';
    const floodAreasTitle = 'Flood Areas';
    const contactUsTitle = 'Contact Us';
    const downloadOurBrochureTitle = 'Download our Brochure';

    return (
        <React.Fragment>
            <Navbar collapseOnSelect fixedTop>
                <Navbar.Header>
                    {/* <Navbar.Brand>
                        <div style={{ background: 'white' }}>
                            <img onClick={e => props.onViewChange(ViewType.overview)}
                                style={{ cursor: 'pointer' }}
                                src="./img/logo.png"
                                className="img-responsive" />
                        </div>
                    </Navbar.Brand> */}
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav bsStyle="tabs">
                        <NavItem
                            active={props.currentView === ViewType.overview}
                            onSelect={e => props.onViewChange(ViewType.overview)}>
                            <strong>{overviewTitle}</strong>
                        </NavItem>
                        <NavItem
                            active={props.currentView === ViewType.floodAreas}
                            onSelect={e => props.onViewChange(ViewType.floodAreas)}>
                            <strong>{floodAreasTitle}</strong>
                        </NavItem>
                        <NavItem
                            active={props.currentView === ViewType.contactUs}
                            onSelect={e => props.onViewChange(ViewType.contactUs)}>
                            <strong>{contactUsTitle}</strong>
                        </NavItem>
                        <NavItem
                            href="docs/floodprotect-product-info.pdf"
                            download="floodprotect-product-info.pdf">
                            <strong>{downloadOurBrochureTitle}</strong>
                        </NavItem>
                        {/* <NavItem>
                            <strong>{'||'}</strong>
                        </NavItem> */}
                        {/* <NavItem>
                            <strong>{'UK Flood Warnings: '}</strong>
                        </NavItem> */}
                    </Nav>
                    {/* <Nav style={{ backgroundColor: ' white' }}>
                        <form className="navbar-form navbar-left" >
                            <div className="form-group">
                                <div
                                    className="label label-warning"
                                >
                                    UK Flood Warnings
                                </div>
                                <div className="form-group">
                                    <FloodWarningsTicker />
                                </div>
                            </div>
                        </form>
                    </Nav> */}
                </Navbar.Collapse>
            </Navbar>
            <ToastContainer />
        </React.Fragment>
    );
}

export default Menubar;
