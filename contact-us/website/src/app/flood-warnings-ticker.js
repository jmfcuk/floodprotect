import React from 'react';
import {
    Modal, Button
} from 'react-bootstrap';
import './flood-warnings-ticker.css';
import { baseApiUrl } from '../global';
import Ticker from 'react-ticker';
// var uniqueid = require('uniqid');
var moment = require('moment');

export default class FloodWarningsTicker extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            fws: null,
            current: 0,
            selected: null
        };
    }

    componentDidMount() {

        this.getFloodWarnings();
            
            // () => {
            // const { fws, current } = this.state;
            // let idx = current;

            // this.interval = setInterval(() => {
            //     if (fws && fws.items) {
            //         if (idx >= (fws.items.length - 1)) {
            //             idx = 0;
            //         } else {
            //             idx++;
            //         }

            //         this.setState({ current: idx });
            //     }
            // }, 2500);
        //});
    }

    // componentWillUnmount() {
    //     clearInterval(this.interval);
    // }

    getFloodWarnings = () => {

        fetch(baseApiUrl + '/fa-fw.php?op=fw', {
              mode: 'cors',
              headers: {
                'Accept': 'application/json',
            }
        })
            .then(response => {
                return response.json();
            })
            .then(fw => {
                this.setState({ fws: JSON.parse(fw) }, () => {
                    // if (cb) {
                    //     cb();
                    // }
                });
            });
    }

    itemSelect = (item) => {
        this.setState({ selected: item });
    }

    itemClose = () => {
        this.setState({ selected: null });
    }

    formatDateTime = (dt) => {

        let m = moment(dt);

        return m.format('dddd DD MMMM YYYY HH:mm:ss');
    }

    render() {

        //const { selected, fws, current } = this.state;

        //let content = null;

        // if (fws && fws.items) {
        //     if (fws.items.length > 0) {
        //         content =
        //             (
        //                 <div
        //                     style={
        //                         {
        //                             cursor: 'pointer',
        //                             backgroundColor: 'white',
        //                             overflow: 'hide'
        //                         }
        //                     }
        //                     onClick={e => this.itemSelect(fws.items[current])}
        //                 >
        //                     &nbsp;&nbsp;{fws.items[current].description}
        //                 </div>
        //             );
        //     } else {
        //         content = (<span>&nbsp;&nbsp;There are no UK flood warnings right now</span>);
        //     }
        // } else {
        //     content = (<span>&nbsp;&nbsp;No UK flood warning data is available right now</span>);
        // }

        return (
            <Ticker className="ticker">
                {({ index }) => (
                    <div>
                        <span>{'Test'}{index}</span>
                    </div>
                )}
            </Ticker>
            // <React.Fragment>
            //     {content}
            //     <Modal show={selected} onHide={this.itemClose}>
            //         <Modal.Header closeButton={true}>
            //             <Modal.Title>{selected && selected.description && selected.description}</Modal.Title>
            //         </Modal.Header>
            //         <Modal.Body>
            //             <div>
            //                 <strong>
            //                     {selected && selected.description && selected.description}
            //                 </strong>
            //             </div>
            //             <div>
            //                 {selected && selected.message && selected.message}
            //             </div>
            //             <div>
            //                 <i>This uses Environment Agency flood and river level data from the real-time data API (Beta)</i>
            //             </div>
            //         </Modal.Body>
            //         <Modal.Footer>
            //             <Button onClick={this.itemClose}>Close</Button>
            //         </Modal.Footer>
            //     </Modal>
            // </React.Fragment>
        );
    }
}

