import React from 'react';
import {
    Row,
    Col,
    Jumbotron,
    Collapse
} from 'react-bootstrap';
import './sub-heading.css';
//import FloodWarningsTicker from '../areas-warnings/tickers/flood-warnings-ticker';

export const SubHeading = props => {

    return (
        <React.Fragment>
            <Jumbotron style={{ paddingTop: 90, paddingLeft: 10, paddingRight: 10}}>
                <Row>
                    <Col xs={12} md={6} className="text-center" style={{ background: 'white' }}>
                        <br /><br />
                        <h1>{props.title}</h1>
                        <br />
                        <h3>
                            <p>{props.blurb}</p>
                        </h3>
                        <br />
                        <h4>
                            {props.footer}
                        </h4>
                    </Col>
                    {props.showImage &&
                        <Col xs={12} md={6}>
                            <img className="img-responsive" 
                            src="./img/logo-cropped.png"
                            style={{borderWidth: 2, borderColor: 'black'}} />
                        </Col>
                    }
                </Row>
            </Jumbotron>
            {/* <Jumbotron>
                <Row>
                    <Col xs={12} md={6}>
                        <h1>{props.title}</h1>
                        <br />
                        <h3>
                            <p>{props.blurb}</p>
                        </h3>
                        <br />
                        <div>
                            {props.footer}
                        </div>
                    </Col>
                    {props.showImage &&
                        <Col xs={12} md={6}>
                            <img className="img-responsive center-block" src="./img/logo.png" />
                        </Col>
                    }
                </Row>
            </Jumbotron> */}
        </React.Fragment>
    );
}

export default SubHeading;
