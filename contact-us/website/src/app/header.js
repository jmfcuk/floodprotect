import React from 'react';
import { Row, Col } from 'react-bootstrap';

const Header = () => {

    return (
            <Row className="text-center">
                <Col xs={2}>
                </Col>
                <Col xs={8}>
                    <h1>Floodprotect</h1>
                    <h4><u>Keeping water where it belongs</u></h4>
                </Col>
                <Col xs={2}>
                    <img className="img-responsive" src="./img/logo.png" />
                </Col>
            </Row>
    );
};

export default Header;
