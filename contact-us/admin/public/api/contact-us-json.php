<?php

    header("Access-Control-Allow-Origin: https://floodprotect.co/beta/admin");
    header("Content-Type: application/json");

    $from = $_GET['from'];
    $to = $_GET['to'];

    // $server = "localhost";
    // $db = "test_db";
    // $uid = "john";
    // $pwd = "john";

    $server = "localhost:3306";
    $db = "reabourn_floodprotect_beta";
    $uid = "reabourn_fp-db";
    $pwd = "fpdbpwd123!";

    $pdo = new PDO("mysql:host=$server;dbname=$db", $uid, $pwd);

    $sql = "SELECT * FROM contact_us WHERE received BETWEEN :from AND :to;";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(":from", $from);
    $stmt->bindParam(":to", $to);
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
?>
